BEGIN;

CREATE SCHEMA ddpo;
GRANT USAGE ON SCHEMA ddpo TO public;

SET search_path TO ddpo, apt, public;


-- BUGS

CREATE TABLE bts_summary (
	package text PRIMARY KEY,
	rc_total integer NOT NULL,
	rc_merged integer NOT NULL,
	in_total integer NOT NULL,
	in_merged integer NOT NULL,
	mw_total integer NOT NULL,
	mw_merged integer NOT NULL,
	fp_total integer NOT NULL,
	fp_merged integer NOT NULL,
	patch_total integer NOT NULL,
	patch_merged integer NOT NULL
);

GRANT SELECT ON bts_summary TO public;


-- POPCON

CREATE TABLE ddpo.popcon (
	package text PRIMARY KEY,
	inst integer NOT NULL,
	vote integer NOT NULL,
	"old" integer NOT NULL,
	recent integer NOT NULL,
	no_files integer NOT NULL
);

DROP VIEW IF EXISTS ddpo.popcon_source;
CREATE OR REPLACE VIEW ddpo.popcon_source AS
	SELECT s.package, MAX(inst) AS inst, MAX(vote) AS vote, MAX("old") AS "old",
		MAX(recent) AS recent, MAX(no_files) AS no_files
	FROM apt.package s
	JOIN apt.package_source ps ON (s.package_id = ps.source_id)
	JOIN apt.package p ON (ps.package_id = p.package_id)
	JOIN ddpo.popcon ON (p.package = popcon.package)
	JOIN apt.packagelist pl ON (p.package_id = pl.package_id)
	JOIN apt.suite USING (suite_id)
	WHERE s.pkg_architecture = 'source' AND
		archive = 'debian' AND
		suite IN ('sid', 'experimental')
	GROUP BY s.package;

GRANT SELECT ON popcon, popcon_source TO public;


-- INCOMING ET AL

CREATE TABLE dak_queue (
	suite_id integer NOT NULL REFERENCES suite,
	queue text NOT NULL,
	package text NOT NULL,
	version text NOT NULL,
	maintainer integer REFERENCES maintainer,
	changed_by integer REFERENCES maintainer (maintainer),
	signed_by integer REFERENCES maintainer (maintainer),
	date timestamp with time zone,
	url text,
	PRIMARY KEY (package, queue) -- package first
);

GRANT SELECT ON dak_queue TO public;


-- WNPP

CREATE TABLE ddpo.wnpp_rm (
	bug integer NOT NULL,
	package text NOT NULL,
	tag text NOT NULL,
	title text NOT NULL,
	submitter text NOT NULL,
	owner text
);

GRANT SELECT ON wnpp_rm TO public;


-- DEHS

CREATE TABLE ddpo.dehs (
	package text NOT NULL,
	suite text NOT NULL,
	up_version text,
	dversion text,
	dversionmangled text,
	uptodate boolean,
	wwiz text,
	class text,
	PRIMARY KEY (package, suite)
);

GRANT SELECT ON dehs TO public;


-- DDPO

DROP VIEW IF EXISTS ddpo.package_versions;
CREATE OR REPLACE VIEW ddpo.package_versions AS
	SELECT
	package, distribution, component, version,
	apt.maint_name (maintainer) AS maintainer,
	uploaders (package_id),
	section, priority, dm_upload_allowed,
	apt.maint_name (changed_by) AS changed_by,
	apt.maint_name (signed_by) AS signed_by,
	date::date, NULL AS url

	FROM package
	JOIN packagelist USING (package_id)
	JOIN suite USING (suite_id)
	JOIN distribution USING (archive, suite)
	LEFT JOIN source USING (package_id)
	WHERE pkg_architecture = 'source'
UNION
	SELECT
	package, distribution ||'-'|| queue, component, version,
	apt.maint_name (maintainer) AS maintainer,
	NULL AS uploaders,
	NULL AS section, NULL AS priority, NULL AS dm_upload_allowed,
	apt.maint_name (changed_by) AS changed_by,
	apt.maint_name (signed_by) AS signed_by,
	date::date, url

	FROM dak_queue
	JOIN suite USING (suite_id)
	JOIN distribution USING (archive, suite);

GRANT SELECT ON package_versions TO public;


COMMIT;
