#!/usr/bin/perl -w

# Copyright (C) 2005, 2006, 2008, 2010 Christoph Berg <myon@debian.org>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# Extract uploads to unstable and experimental from projectb.
#
# DB format:
# nmu:<login>@debian.org : <list of NMU uploads to unstable/experimental>
# qa:<login>@debian.org : <list of QA uploads to unstable/experimental>
# <login>@debian.org : <list of remaining uploaded packages to unstable/experimental>
# ud:<package> : <last upload date (for sorting)>
# ud:<package>:<version> : <upload date>
# ul:<package>:<version> : <uploader>
# ch:<package>:<version> : <date> by <changedby> [(Uploader: <uploader>)]

use strict;
use DBD::Pg;
use DB_File;

# mouseover title
sub mouseover_date
{
    my ($date, $changedby, $uploader) = @_;
    $date =~ s/^.*?(\d{1,2} \w{3} \d{4}).*/$1/;
    my $ret = "$date by $changedby";
    $uploader =~ /(.*)@/;
    my $uploader_localpart = $1 || $uploader;
    if ($changedby !~ /<$uploader_localpart\@/) { # assume matching local part implies same person
	$ret .= " (Uploader: $uploader)";
    }
    $ret =~ s/&/&amp;/g; # remove meta chars
    $ret =~ s/"/&quot;/g;
    return $ret;
}

my $db_filename = "uploads-new.db";

my %pkglist;
my %db_content;

my $db = tie %db_content, "DB_File", $db_filename, O_RDWR|O_CREAT|O_TRUNC, 0666, $DB_BTREE
	or die "Can't open database $db_filename : $!";

my %carnivore;
my $carnivore_db = tie %carnivore, "DB_File", "carnivore.db", O_RDONLY, 0666, $DB_BTREE
	or die "Can't open database carnivore.db : $!";

my $projectb = DBI->connect ("dbi:Pg:service=projectb", "", "");
$projectb->do ("SET client_encoding = 'utf-8'");

my $query = "
SELECT source.source, version, suite,
	install_date,
	to_char (install_date, 'DD Mon YYYY') AS date,
	fingerprint,
	CASE WHEN uid.uid ~ '\@' THEN uid.uid ELSE uid.uid||'\@debian.org' END,
	maint.name AS maintainer, changer.name AS changedby
FROM source
JOIN src_associations ON source.id = src_associations.source
JOIN maintainer maint ON source.maintainer = maint.id
LEFT JOIN maintainer changer ON source.changedby = changer.id
JOIN fingerprint ON source.sig_fpr = fingerprint.id
LEFT JOIN uid ON fingerprint.uid = uid.id";
my $sth = $projectb->prepare ($query);
$sth->execute ();

while (my ($source, $version, $suite, $install_date, $date, $fpr, $uid, $maint, $changedby) = $sth->fetchrow_array) {
	$fpr = substr($fpr, -8, 8);
	$db_content{"ul:$source:$version"} = $uid || "0x$fpr";
	$db_content{"ch:$source:$version"} = mouseover_date
		($date || '??', $changedby || '??', $uid || "0x$fpr");
	if ($install_date) {
		$db_content{"ud:$source:$version"} = $install_date;
		if (not $db_content{"ud:${source}"}
				or $install_date gt $db_content{"ud:${source}"}) {
			$db_content{"ud:${source}"} = $install_date;
		}
	}
	# 1=experimental, 5=unstable, 4 = testing, 6 = t-p-u
	# (coding that into the query makes it horribly slow)
	next unless $suite == 1 or $suite == 5 or $suite == 4 or $suite == 6;

	my $changer = $changedby;
	$changer = $1 if $changedby =~ /<(.+)>/;

	my $section = "";
	if ($version =~ /(-\d+\.|\+nmu)\d+$/) {
		$section = "nmu:"
	} elsif ($maint =~ /<packages\@qa.debian.org>/) {
		$section = "qa:"
	}

	my $uploadersection = "";
	$uploadersection = $section
		if( ! $uid
		or $changer eq $uid
		or defined $carnivore{"email2id:$changer"} and defined $carnivore{"email2id:$uid"}
			and $carnivore{"email2id:$changer"} == $carnivore{"email2id:$uid"} );

	# Changer
	push @{$pkglist{"$section$changer"}}, $source;

	# Uploader
	push @{$pkglist{lc "${uploadersection}0x$fpr"}}, $source;
	push @{$pkglist{"$uploadersection$uid"}}, $source
		if( $uid and ( $uid ne $changer or $uploadersection ne $section ) );

	#print "$source $section $uploadersection $changer $uid\n" if( $changer ne $uid and $section ne "" );
}

foreach (keys %pkglist) {
	$db_content{$_} = join ' ', sort @{$pkglist{$_}};
}

sub remove_duplicates
{
	my $value = shift;

	$value =~ s/^ *//;
	$value =~ s/ *$//;
	$value =~ s/  */ /g;

	my %entry;

	foreach my $entry ( split( / /, $value ) )
	{
		$entry{$entry} = 1;
	}

	$value = "";

	foreach my $entry ( sort keys %entry )
	{
		$value .= " $entry";
	}

	$value =~ s/^ *//;

	return $value;
}

foreach my $key ( keys %db_content )
{
	next if( $key =~ /^([^:]+:)?0x[0-9a-f]{8}$/ );
	next if( $key =~ /:/ and $key !~ /^(nmu|qa):/ );

	my $key_lc = lc( $key );
	next if( $key_lc eq $key );

	my $value = $db_content{$key};
	$value .= " " . $db_content{$key_lc} if( defined $db_content{$key_lc} );

	$db_content{$key_lc} = remove_duplicates( $value );
	delete $db_content{$key};
}

