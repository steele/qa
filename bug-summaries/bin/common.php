<?php

function readbugs($buglist, $pkgs, $file) {
  $fp = @fopen(BUGS_DIR."/".$file, "r") or die ("Kann Datei nicht lesen.");
  $allbugs = array();

  while ($line = fgets($fp, 1024)) {
  #print "<br>input: $line<br>";
    if ((preg_match("/^ (.*)/", $line, $x)) && ($l)) {
      $c[$l] = $c[$l].$x[1];
    }
    else {
      $l="";
  
      if (preg_match("/^debbugsAffected: (.*)/i",$line, $x))
        $l='affected';
      else if (preg_match("/^debbugsPackage: (.*)/i",$line, $x))
#        if (($x[1] != "ftp.debian.org") && ($x[1] != "qa.debian.org"))
          $l='p';
      if ($l) {
        if (!($c{$l})) $c{$l}=array();
        array_push($c{$l}, $x[1]);
#        print "$l pushed ".$x[1]."#$line#\n";
  #      print $c{'p'}[0];
      }
      $l="";
      
      if (preg_match("/^debbugsPackage: (.*)/i",$line, $x))
        if ($x[1] == "ftp.debian.org")
          $l='RM';
        else if ($x[1] == "qa.debian.org")
          $l='RM';
      if (preg_match("/^debbugsTitle: (.*)/i",$line, $x))
        $l='t';
      else if (preg_match("/^debbugsTitle:: (.*)/i",$line, $x))
        $l='tt';
      else if (preg_match("/^debbugsSeverity: (.*)/i",$line, $x))
        $l='severity';
      else if (preg_match("/^debbugsID: (.*)/i",$line, $x))
        $l='i';
      else if (preg_match("/^debbugsMergedWith: (.*)/i",$line, $x))
        { if (!$c{'='} || ($c{'='} > $x[1])) $c{'='}=$x[1]; }
      else if (preg_match("/^debbugsTag: pending/i",$line))
        $c{'P'}=1;
      else if (preg_match("/^debbugsTag: patch/i",$line))
        $c{'+'}=1;
      else if (preg_match("/^debbugsTag: help/i",$line))
        $c{'H'}=1;
      else if (preg_match("/^debbugsTag: moreinfo/i",$line))
        $c{'M'}=1;
      else if (preg_match("/^debbugsTag: unreproducible/i",$line))
        $c{'R'}=1;
      else if (preg_match("/^debbugsTag: security/i",$line))
        $c{'S'}=1;
      else if (preg_match("/^debbugsTag: upstream/i",$line))
        $c{'U'}=1;
      else if (preg_match("/^debbugsTag: wontfix/i",$line))
        $c{'wontfix'}=1;
      else if (preg_match("/^debbugsTag: etch/i",$line))
        $c{'T'}=1;
      else if (preg_match("/^debbugsTag: sid/i",$line))
        $c{'TT'}=1;
      else if ((preg_match("/^debbugsTag: fixed$/i",$line)) || (preg_match("/^debbugsState: done/i",$line)))
        $c{'fixeddone'}=1;
      else if (preg_match("/^debbugsDate: (.*)/i",$line, $x))
        $l = 'date';
  
      if ($l)
        $c{$l} = $x[1];
    }
  
    if ((preg_match("/^$/", $line) && ($c['i']))) { # && (dba_exists($c['p'], $binTesting))) {
  #    print "processing ".$c['p']."<br>";
  
      if($c['tt']) $c['t']=base64_decode($c['tt']);
  
      if ($closes[$c['i']])
        $c['D']=$closes[$c['i']];
      if ($claim[$c['i']])
        $c['C']=$claim[$c['i']];
  
      if (($c['TT']) && (!($c['T']))) $c['B']=1;
      if($c['TT']) unset($c['T']);
  
      if (($c['=']) && ($c['='] >= $c{'i'})) unset($c['=']);

      $countthis=0;
      if ($pkgs) {
          if ($c{'p'}) foreach ($c{'p'} as $pkg) {
            if (! dba_exists($pkg, $pkgs)) continue;
            $countthis=1;
          }
      } else
          $countthis=1;
      if (!$countthis) $c{'-'} = 1;
      
      
      dba_replace($c{'i'}, serialize($c), $buglist);
      
      if ($c{'p'}) foreach ($c{'p'} as $pkg) {
          if (!($packagebugs[$pkg])) $packagebugs[$pkg]=array();
          array_push($packagebugs[$pkg],$c{'i'});
          if (($pkgs) && ! dba_exists($pkg, $pkgs)) continue;
          # $srcpkg = dba_fetch($pkg, $pkgs);
          #if (!($srcpackagebugs[$srcpkg])) $srcpackagebugs[$srcpkg]=array();
          #array_push($srcpackagebugs[$srcpkg],$c{'i'});
      }
      array_push($allbugs,$c{'i'});
      $c=array();
    }
  }
  
  fclose($fp);
  
  dba_replace('allbugs', serialize($allbugs), $buglist);
  dba_replace('packagebugs', serialize($packagebugs), $buglist);
  #dba_replace('srcpackagebugs', serialize($srcpackagebugs), $buglist);
}
  
?>
