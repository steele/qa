#!/usr/bin/perl
# +-------------------------------------------------------------------------+
# | Copyright (C) 2002 Igor Genibel http://genibel.org/                     |
# | Copyright (C) 2005-2009 Christoph Berg <myon@debian.org>                |
# |                                                                         |
# | This program is free software; you can redistribute it and/or           |
# | modify it under the terms of the GNU General Public License             |
# | as published by the Free Software Foundation; either version 2          |
# | of the License, or (at your option) any later version.                  |
# |                                                                         |
# | This program is distributed in the hope that it will be useful,         |
# | but WITHOUT ANY WARRANTY; without even the implied warranty of          |
# | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           |
# | GNU General Public License for more details.                            |
# +-------------------------------------------------------------------------+

use warnings;
use strict;
use CGI qw(:standard);
use CGI::Carp qw(warningsToBrowser fatalsToBrowser);
use DBD::Pg;
use HTML::Entities;
use Template;
use Time::HiRes qw(time);
use URI::Escape;

my $start_time = time;

$CGI::POST_MAX = 0;
$CGI::DISABLE_UPLOADS = 1;

my $cgi = new CGI;
my $dbh = DBI->connect("dbi:Pg:service=qa;user=guest", '', '',
	{ AutoCommit => 1, RaiseError => 1, PrintError => 1});
$dbh->do ("SET search_path TO ddpo, apt, public");

# global variables
my %config = (
	bugs => 1,
	version => 1,
	ubuntu => 1,
	excuses => 1,
	bin => 1,
	buildd => 1,
	debcheck => 1,
	popc => 1,
	watch => 1,
	section => 1,
	mirror => "https://deb.debian.org/debian",
	ordering => 0,
	secordering => 0,
	uploads => 1,
);

#####################################################################################

sub pool_prefix ($)
{
	my $package = shift;
	return $1 if ($package =~ /^(lib.)/);
	return substr ($package, 0, 1);
}

sub bug_cell ($$$$)
{
	my ($bugs, $package, $uripackage, $severity) = @_;
	my $bugsurl = "https://bugs.debian.org/cgi-bin/pkgreport.cgi?src=$uripackage";
	if (not $bugs->{$package}->{"${severity}_total"}) {
		return td(a({-href => $bugsurl, -class => "dash"},
				"-"));
	}
	my $b = a({-href => $bugsurl},
		  $bugs->{$package}->{"${severity}_merged"});
	if (($bugs->{$package}->{"${severity}_total"} || 0) >
	    ($bugs->{$package}->{"${severity}_merged"} || 0)) {
		$b .= "<br />(" .
			a({-href => "$bugsurl;repeatmerged=yes"},
			  $bugs->{$package}->{"${severity}_total"}) .
		      ")";
	}
	return td($b);
}

sub debcheck ($$$)
{
	my ($suite, $package, $uripackage) = @_;
	my $packagefile = $package;
	$packagefile =~ s/\./_/; # no /g !
	if (-f "/srv/qa.debian.org/data/debcheck/result/$suite/packages/$packagefile") {
		return td(a({-href => "/debcheck.php?dist=$suite&package=$uripackage"},
			    ucfirst (substr ($suite, 0, 2))));
	} else {
		return td("-");
	}
}

my $dehs_q = $dbh->prepare ("SELECT *,
	COALESCE (up_version, wwiz) > dversionmangled AS behind,
	COALESCE (up_version, wwiz) < dversionmangled AS ahead
	FROM dehs WHERE package = ? AND suite = ?");

sub dehs ($$$) {
	my ($suite, $package, $uripackage) = @_;
	$dehs_q->execute ($package, $suite);
	my $row = $dehs_q->fetchrow_hashref;
	return td ('-') unless ($row);
	my $up_version = $row->{up_version};
	if (not $up_version and $row->{wwiz}) {
		$up_version = $row->{wwiz} if
			($row->{wwiz} ne 'error' and $row->{wwiz} ne 'notmatch' and $row->{wwiz} ne 'no_cright');
	}
	$up_version = '-' unless ($up_version);
	return td (a ({-href => "https://dehs.alioth.debian.org/report.php?package=$uripackage"},
			span ({-class => $row->{class},
				-title => ($row->{class} ? "Debian version is $row->{class}" : "")},
				$up_version)));
}

#####################################################################################

my $package_q = $dbh->prepare ("SELECT * FROM package_versions WHERE package = ?");
my $bugs_q = $dbh->prepare ("SELECT *,
	rc_total + in_total + mw_total + fp_total AS bugs_total,
	rc_merged + in_merged + mw_merged + fp_merged AS bugs_merged
	FROM bts_summary WHERE package = ?");
my $binaries_q = $dbh->prepare ("SELECT DISTINCT p.package,
		apt.package_info(p.package_id, 'Description') AS description
	FROM apt.package s
	JOIN apt.package_source ps ON (s.package_id = ps.source_id)
	JOIN apt.package p ON (ps.package_id = p.package_id)
	JOIN apt.packagelist pl ON (p.package_id = pl.package_id)
	JOIN apt.suite USING (suite_id)
	JOIN apt.distribution USING (archive, suite)
	WHERE (s.package, s.pkg_architecture) = (\$1, 'source') AND
		archive = 'debian' AND
		distribution IN ('testing', 'unstable', 'experimental')");
my $arch_all_q = $dbh->prepare ("SELECT 'all' = ALL (SELECT p.pkg_architecture
	FROM apt.package s
	JOIN apt.package_source ps ON (s.package_id = ps.source_id)
	JOIN apt.package p ON (ps.package_id = p.package_id)
	JOIN apt.packagelist pl ON (p.package_id = pl.package_id)
	JOIN apt.suite USING (suite_id)
	WHERE (s.package, s.pkg_architecture) = (\$1, 'source') AND
		archive = 'debian' AND
		suite IN ('sid', 'experimental'))");
my $popcon_q = $dbh->prepare ("SELECT * FROM popcon_source WHERE package = ?");

my $dists = [
{ name => 'oldstable',
  subdists => [
    { name => "oldstable-incoming", class => "incoming" },
    { name => "oldstable-security", class => "security" },
    { name => "oldstable-proposed-updates", class => "proposed-updates" },
    { name => "oldstable-proposed-updates-new", class => "proposed-updates-new" },
    { name => "oldstable-backports", class => "bpo", url => "http://ftp.debian.org/debian/pool/" },
  ],
},
{ name => 'stable',
  subdists => [
    { name => "stable-incoming", class => "incoming" },
    { name => "stable-security", class => "security" },
    { name => "stable-proposed-updates", class => "proposed-updates" },
    { name => "stable-proposed-updates-new", class => "proposed-updates-new" },
    { name => "stable-backports", class => "bpo", url => "http://ftp.debian.org/debian/pool/" },
  ],
},
{ name => 'testing',
  subdists => [
    { name => "testing-incoming", class => "incoming" },
    { name => "testing-security", class => "security" },
    { name => "testing-proposed-updates", class => "proposed-updates" },
  ],
},
{ name => 'unstable',
  subdists => [
    { name => "unstable-incoming", class => "incoming" },
    { name => "unstable-deferred", class => "deferred" },
    { name => "unstable-new", class => "new" },
  ],
},
{ name => 'experimental',
  subdists => [
    { name => "experimental-incoming", class => "incoming" },
    { name => "experimental-deferred", class => "deferred" },
    { name => "experimental-new", class => "new" },
  ],
}
];

sub print_package_header ()
{
	my $b = "<tr>";
	$b .= th({-rowspan => 2}, span("Package"));
	$b .= th({-colspan => 5}, span("Bugs")) if ($config{bugs});
	$b .= th({-colspan => 5}, span("Version")) if ($config{version});
	$b .= th({-rowspan => 2}, span("Excuses")) if ($config{excuses});
	$b .= th({-rowspan => 2}, span("Binary<br />Package")) if ($config{bin});
	$b .= th({-rowspan => 2}, span("Buildd")) if ($config{buildd});
	$b .= th({-colspan => 3}, span("Debcheck")) if ($config{debcheck});
	$b .= th({-rowspan => 2}, span("Popcon<br />Inst")) if ($config{popc});
	$b .= th({-colspan => 2}, span("Watch")) if ($config{watch});
	$b .= th({-rowspan => 2}, span("Section<br />Priority")) if ($config{section});
	$b .= "</tr>\n";

	$b .= "<tr>";
	# bugs
	$b .= th(span("All")) .
	      th(span("RC")) .
	      th(span("I&N")) .
	      th(span("M&W")) .
	      th(span("F&P")) if ($config{bugs});
	# version
	$b .= th(span("Oldstable")) .
	      th(span("Stable")) .
	      th(span("Testing")) .
	      th(span("Unstable")) .
	      th(span("Exp")) if ($config{version});
	# debcheck
	$b .= th(span("St")) .
	      th(span("Te")) .
	      th(span("Un")) if ($config{debcheck});
	# watch
	$b .= th(span("Unst")) .
	      th(span("Exp")) if ($config{watch});
	$b .= "</tr>\n";
}

sub package_row ($$)
{
	my ($component, $package) = @_;
	my (%data, %binary, %bugs);

	$package_q->execute ($package);
	while (my $row = $package_q->fetchrow_hashref) {
		$data{$row->{distribution}} = $row;
	}
	$package_q->finish;

	my $description;
	$binaries_q->execute ($package);
	while (my $row = $binaries_q->fetchrow_hashref) {
		$binary{$row->{package}} = $row;
		# source description is first description, or description of
		# binary with same name
		if (not $description or $row->{package} eq $package) {
			$description = $row->{description};
		}
	}
	$binaries_q->finish;

	$bugs_q->execute ($package);
	while (my $row = $bugs_q->fetchrow_hashref) {
		$bugs{$row->{package}} = $row;
	}
	$bugs_q->finish;

	my $pool_prefix = pool_prefix ($package);
	my $uripackage = uri_escape ($package);

	# package name
	my $comaintainers = 0; # DBD::Pg stupidity: {} is really {""}
	$comaintainers = @{$data{experimental}->{uploaders}}
		if $data{experimental}->{uploaders} and $data{experimental}->{uploaders}->[0];
	$comaintainers = @{$data{unstable}->{uploaders}}
		if $data{unstable}->{uploaders} and $data{unstable}->{uploaders}->[0];
	my $b .= td({-class => 'packagenamecell'},
		a({-href => "https://tracker.debian.org/pkg/$uripackage",
		     -class => 'packagename',
		     -title => $description},
			$package) .
		($comaintainers ?
			span({-title => "$comaintainers co-maintainer" .
					($comaintainers > 1 ? 's' : '')},
				'*')
			: '') .
		"<br >" .
		"&nbsp;&nbsp;" .
		a({-href => "$config{mirror}/pool/$component/$pool_prefix/$uripackage/"},
			"Pool")
	);

	# bugs
	$b .= bug_cell (\%bugs, $package, $uripackage, "bugs") .
	      bug_cell (\%bugs, $package, $uripackage, "rc") .
	      bug_cell (\%bugs, $package, $uripackage, "in") .
	      bug_cell (\%bugs, $package, $uripackage, "mw") .
	      bug_cell (\%bugs, $package, $uripackage, "fp")
		if $config{bugs};

	# versions
	my ($cell, $colspan, $cur_ver) = ("", 0, "");
	my $maxdate = "1970-01-01";
	foreach my $dist (@$dists) {
		my $name = $dist->{name};
		my $version = $data{$name}->{version} || "";
		# print information from previous dists
		if ($colspan and $version ne $cur_ver) {
			$b .= td ({-colspan => $colspan}, $cell)
				if ($config{version});
			($cell, $colspan) = ("", 0);
		}
		$cur_ver = $version;

		# dist version
		my $title = $name;
		if ($data{$name}->{date} and $data{$name}->{changed_by}) {
			$title .= ": $data{$name}->{date} by $data{$name}->{changed_by}";
			if ($data{$name}->{signed_by} and
					$data{$name}->{signed_by} ne $data{$name}->{changed_by}) {
				if ("$data{$name}->{changed_by};$data{$name}->{signed_by}" =~ /(.*) <.*>;(.*) <.*>/) {
					$title .= " (Uploader: $data{$name}->{signed_by})"
						if (lc($1) ne lc($2));
						# add uploader info if real names differ
				}
			}
			$maxdate = $data{$name}->{date} if ($data{$name}->{date} gt $maxdate);
		}
		$cell = $version ? span({-class => $name, -title => $title},
					$data{$name}->{version}) : '-'
			unless ($cell); # use class and title of first dist

		# subdist versions
		foreach my $subdist (@{$dist->{subdists}}) {
			my $s = $subdist->{name};
			if ($data{$s}->{version}) {
				# skip p-u display if security has that version, etc.
				next if ($version eq $data{$s}->{version});
				$cell .= "<br />";
				my $span = span ({-class => $subdist->{class}, -title => $s},
						$data{$s}->{version});
				my $url = ($data{$s}->{url} || $subdist->{url});
				if ($url) {
					$url =~ s{/pool/$}{/pool/$component/$pool_prefix/$uripackage/};
					$cell .= a ({-href => $url}, $span);
				} else {
					$cell .= $span;
				}
				$version = $data{$s}->{version};
			}
		}
		$colspan++;
	}
	$b .= td({-colspan => $colspan}, $cell)
		if $config{version};

	# excuses
	if (not $config{excuses}) {
		# skip
	} elsif ($data{unstable}->{version} and
		$data{unstable}->{version} ne ($data{testing}->{version}||'')) {
		$b .= td(
			a({-href => "https://qa.debian.org/excuses.php?package=$uripackage"},
				"Excuses")
		);
	} else {
		$b .= td("-");
	}

	# binaries
	if (not $config{bin}) {
		# skip
	} elsif (keys %binary) {
		my $c = 1;
		my $cell = join ' ',
			map { a({-href => "https://packages.debian.org/" . uri_escape ($_),
				 -title => "$_ - $binary{$_}->{description}"},
				$c++)
			} keys %binary;
		$b .= td($cell);
	} else {
		$b .= td("-");
	}

	# buildd
	$arch_all_q->execute ($package);
	my ($arch_all) = $arch_all_q->fetchrow_array;
	$arch_all_q->finish;
	if (not $config{buildd}) {
		# skip
	} else {
		$b .= td(
			a({-href => "https://buildd.debian.org/build.php?pkg=$uripackage"},
				"Buildd") .
			"<br />" .
			a({-href => "https://buildd.debian.org/build.cgi?pkg=$uripackage"},
				"More")
		);
	}

	# debcheck
	$b .= debcheck ("stable", $package, $uripackage) .
	      debcheck ("testing", $package, $uripackage) .
	      debcheck ("unstable", $package, $uripackage)
		if ($config{debcheck});

	# popcon
	$popcon_q->execute($package);
	my $row = $popcon_q->fetchrow_hashref;
	$popcon_q->finish;
	if (not $config{popc}) {
		# skip
	} elsif ($row->{inst}) {
		$b .= td(a({-href => "/popcon.php?package=$uripackage"},
			   $row->{inst}));
	} else {
		$b .= td("-");
	}

	# DEHS
	$b .= dehs ("unstable", $package, $uripackage) .
	      dehs ("experimental", $package, $uripackage)
		if ($config{watch});

	# section/priority
	if (not $config{section}) {
		# skip
	} elsif ($data{unstable}->{section}) {
		$b .= td($data{unstable}->{section} . "<br />" .
			($data{unstable}->{priority} || '-'));
	} elsif ($data{experimental}->{section}) {
		$b .= td($data{experimental}->{section} . "<br />" .
			($data{experimental}->{priority} || '-'));
	} else {
		$b .= td("-");
	}

	return { td => $b, maxdate => $maxdate };
}

#####################################################################################

sub print_packagelist ($$)
{
	my ($component, $packages) = @_;
	my $table = h2("$component (" . scalar (@$packages) . ")") . "\n";
	$table .= "<table class=\"packagetable\">\n";
	$table .= print_package_header ();

	my %row;
	foreach my $package (@$packages) {
		$row{$package} = package_row ($component, $package);
	}

	# re-sort by upload date
	if ($config{ordering} == 1) {
		$packages = [ sort { $row{$b}->{maxdate} cmp $row{$a}->{maxdate}; } @$packages ];
	}

	my $row = 1;
	foreach my $package (@$packages) {
		$table .= Tr ({-class => ($row++ % 2) ? "odd" : "even"},
			  $row{$package}->{td});
	}
	$table .= "</table>\n";
}

#####################################################################################

my $packagelist_q = $dbh->prepare (
"SELECT DISTINCT component, package
	FROM suite
	JOIN packagelist USING (suite_id)
	JOIN package USING (package_id)
WHERE archive = 'debian' AND suite IN ('sid', 'experimental') AND
	package_id IN
		(SELECT package_id FROM source WHERE maintainer = \$1
		 UNION
		 SELECT package_id FROM uploader WHERE maintainer = \$1)");

sub print_developer (@)
{
	my ($id, $name, $login) = @_;
	my $urilogin = uri_escape ($login);
	my $initial = substr ($login, 0, 1);

	my $b = h1(encode_entities($name)) . "\n";
	$b .= "<!-- maintainer $id -->\n";
	$b .= p({-style => "font-size: small"},
		"Bugs: " .
		a({-href => "https://bugs.debian.org/cgi-bin/pkgreport.cgi?which=maint;data=$urilogin;archive=no;raw=yes;bug-rev=yes;pend-exc=fixed;pend-exc=done"},
			"open") . " -\n" .
		a({-href => "https://bugs.debian.org/cgi-bin/pkgreport.cgi?which=maint;data=$urilogin;archive=no;archive=no;pend-exc=done;sev-inc=critical;sev-inc=grave;sev-inc=serious"},
			"RC") . " -\n" .
		a({-href => "https://bugs.debian.org/cgi-bin/pkgreport.cgi?maint=$urilogin"},
			"all") . " -\n" .
		a({-href => "https://bugs.debian.org/cgi-bin/pkgreport.cgi?submitter=$urilogin"},
			"submitted") . " -\n" .
		a({-href => "https://bugs.debian.org/cgi-bin/pkgreport.cgi?owner=$urilogin"},
			"owned") . " -\n" .
		a({-href => "/wnpp.php?login=$urilogin"},
			"WNPP") . "<br />\n" .
		"Reports: " .
		a({-href => "https://buildd.debian.org/pkg.cgi?maint=$urilogin"},
			"Buildd") . " -\n" .
		a({-href => "https://lintian.debian.org/maintainer/$urilogin.html"},
			"Lintian") . " -\n" .
		a({-href => "https://debtags.alioth.debian.org/todo.html?maint=$urilogin"},
			"Debtags") . " -\n" .
		a({-href => "https://dehs.alioth.debian.org/report.php?login=$urilogin"},
			"DEHS") . " -\n" .
		a({-href => "https://piuparts.debian.org/sid/maintainer/$initial/$urilogin.html"},
			"Piuparts") . " -\n" .
		a({-href => "https://contributors.debian.org/contributor/$urilogin/"},
			"Contributions") . " -\n" .
		a({-href => "https://portfolio.debian.net/result?email=$urilogin"},
			"Portfolio"));

	$packagelist_q->execute ($id);
	my %packages;
	while (my $row = $packagelist_q->fetchrow_hashref) {
		push @{$packages{$row->{component}}}, $row->{package};
	}
	$packagelist_q->finish;

	my $found = 0;
	for my $component (qw/main contrib non-free/) {
		if ($packages{$component}) {
			$b .= print_packagelist ($component, $packages{$component});
			$found++;
		}
	}
	unless ($found) {
		$b .= p("No packages in unstable or experimental");
	}

	return ($b, $found);
}

#####################################################################################

my $status = "200 Ok";
my $title = "DDPO";
my $body = '';

# read config
my %cookie = $cgi->cookie('ddpo');
foreach my $item (keys %config) {
	if (defined $cookie{$item}) {
		$config{$item} = $cookie{$item};
	}
	if (defined $cgi->param($item)) {
		$config{$item} = $cgi->param($item);
	}
}

# set new cookie
my $cookie_value;
if (defined $cgi->param('set')) {
	$cookie_value = \%config;
} elsif (keys %cookie) {
	# only refresh old cookie
	$cookie_value = \%cookie;
}
my $new_cookie = cookie (-name => 'ddpo',
			 -value => $cookie_value,
			 -path => '/cgi-bin/ddpo',
			 -expires => '+6M');

if (my $names = $cgi->param('login')) {
	my ($n_devel, $titlename) = (0, '');
	my $q = $dbh->prepare ("SELECT maintainer, name, email_address(name) AS login
		FROM maintainer
		WHERE CASE WHEN position ('\@' IN \$1) > 0
			THEN email_address(name) = \$1
			ELSE name ILIKE '%'||\$1||'%'
		      END
		ORDER BY name LIMIT 10");
	foreach my $name (split /, */, $names) {
		$q->execute ($name);
		while (my @login = $q->fetchrow_array) {
			my ($b, $f) = print_developer (@login);
			$body .= $b;
			$n_devel++ if $f;
			$titlename = $login[1];
		}
		$q->finish;
	}
	if ($n_devel eq 0) {
		$status = "404 No developers found";
		# $body might still contain interesting links
	} elsif ($n_devel eq 1) {
		$title = "DDPO: ${titlename}'s packages";
	}

} else {
	$body = p("Missing parameter");
	$status = "404 Missing parameter";
}

#####################################################################################

print header (-charset => 'utf-8', -status => $status, -cookie => $new_cookie);

my $template = Template->new (INCLUDE_PATH => '/srv/qa.debian.org/web');
my $login = $cgi->param('login');

$template->process('ddpo-template.html', {
	TITLE => $title,
	LOGIN => $login,
	CONFIG => \%config,
	BODY => $body,
	GENERATED => sprintf "Rendering took %.3fs.\n", time - $start_time,
}) || die $template->error();

$dbh->disconnect;
